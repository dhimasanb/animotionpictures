<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

function getMovie($slug)
{
    // Read the JSON file
    $json = file_get_contents(base_path() . '/resources/data/movies.json');

    // Decode the JSON file
    $json_data = json_decode($json, true);

    $item = null;
    foreach ($json_data as $struct) {
        if ($slug == $struct['slug']) {
            $item = $struct;
            break;
        }
    }

    return view('blog-detail')->with($item);
}

function getWork($slug)
{
    // Read the JSON file
    $json = file_get_contents(base_path() . '/resources/data/works.json');

    // Decode the JSON file
    $json_data = json_decode($json, true);

    $item = null;
    foreach ($json_data as $struct) {
        if ($slug == $struct['slug']) {
            $item = $struct;
            break;
        }
    }

    return view('blog-detail')->with($item);
}

// Main Section
Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/career', function () {
    return view('career');
})->name('career');

Route::get('/culture', function () {
    return view('culture');
})->name('culture');

// About Section
Route::get('/innovation', function () {
    return view('abouts.innovation');
})->name('innovation');

Route::get('/cgi', function () {
    return view('abouts.cgi');
})->name('cgi');

Route::get('/new-technique', function () {
    return view('abouts.new-technique');
})->name('new-technique');

Route::get('/vfx-sfx', function () {
    return view('abouts.vfx-sfx');
})->name('vfx-sfx');

Route::get('/beam-rendering', function () {
    return view('abouts.beam-rendering');
})->name('beam-rendering');

// Career Section
Route::get('/current-opening', function () {
    return view('careers.current-opening');
})->name('current-opening');

Route::get('/early-career-program', function () {
    return view('careers.early-career-program');
})->name('early-career');

Route::get('/express-future-interest', function () {
    return view('careers.express-future-interest');
})->name('express-future');

Route::get('/frequently-asked-questions', function () {
    // Read the JSON file
    $json = file_get_contents(base_path() . '/resources/data/faq.json');

    // Decode the JSON file
    $faqs = json_decode($json, true);

    return view('careers.frequently-asked-questions')->with('faqs', $faqs);
})->name('faq');

Route::get('/innovative-technology', function () {
    return view('careers.innovative-technology');
})->name('innovative-technology');

// Culture Section
Route::get('/community', function () {
    return view('cultures.community');
})->name('community');

Route::get('/event', function () {
    return view('cultures.event');
})->name('event');

Route::get('/giving-back', function () {
    return view('cultures.giving-back');
})->name('giving-back');

// Movies Section
Route::get('/banny', function () {
    return getMovie('banny');
});

Route::get('/oncomm', function () {
    return getMovie('oncomm');
});

Route::get('/emot-show', function () {
    return view('blog-emot-detail');
});

// Works Section
Route::get('/muspada-1', function () {
    return getWork('muspada-1');
});

Route::get('/museum-naspro', function () {
    return getWork('museum-naspro');
});

Route::get('/muspada-2', function () {
    return getWork('muspada-2');
});

Route::get('/muspada-3', function () {
    return getWork('muspada-3');
});

Route::get('/mrt', function () {
    return getWork('mrt');
});

Route::get('/kai', function () {
    return getWork('kai');
});

Route::post('/stay-in-touch', function (\Illuminate\Http\Request $request) {
    try {
        Mail::to('animotionspictures@gmail.com')
            ->send(new \App\Mail\StayInTouch($request->all()));

        return redirect('/')->with('success', 'Berhasil dikirim');
    } catch (Exception $e) {
        \Illuminate\Support\Facades\Log::error('Error when submit stay in touch',
            (array)$e);
        return redirect('/')->with('failed', 'Gagal dikirim');
    }
})->name('post.stay-in-touch');

Route::get('/config-clear', function() {
    Artisan::call('config:clear');
    return 'Configuration cache cleared!';
});

Route::get('/config-cache', function() {
    Artisan::call('config:cache');
    return 'Configuration cache cleared! <br> Configuration cached successfully!';
});

Route::get('/cache-clear', function() {
    Artisan::call('cache:clear');
    return 'Application cache cleared!';
});

Route::get('/view-cache', function() {
    Artisan::call('view:cache');
    return 'Compiled views cleared! <br> Blade templates cached successfully!';
});

Route::get('/view-clear', function() {
    Artisan::call('view:clear');
    return 'Compiled views cleared!';
});
