<!--Plugins-->
<script src="js/jquery.js"></script>
<script src="js/plugins.js"></script>
<!--Template functions-->
<script src="js/functions.js"></script>
<script src="js/custom.js"></script>

<script>
    @if (session('success'))
    $.notify({
        message: '{{ session('success') }}'
    }, {
        type: "success",
        delay: 20000
    })
    @endif

    @if (session('failed'))
    $.notify({
        message: '{{ session('failed') }}'
    }, {
        type: "danger",
        delay: 20000
    })
    @endif
</script>
