@extends('layouts.master')
@section('title', 'EXPRESS FUTURE INTEREST')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/express-future/express-futurehead.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>EXPRESS FUTURE INTEREST</b></h2>
            <p>Sebagai perusahaan yang bergerak dalam industri kreatif Animotion juga bergerak dan aktif membangun
                serta
                menghidupkan organisasi kreatif dan membangun generasi kreatif untuk lebih maju kedepannya.
            </p>
            <p>Banyak aktifitas-aktifitas kreatif dan sangat positif yang kami lakukan tidak hanya pelatihan karya
                dan kerja, namun
                juga membantu membangun point - point interest teman - teman dalam organisasi kami. </p>
            <p>Karena kami sangat sadar di perlukan banyak persiapan untuk membangun masa depan , baik ketika
                bertemu kesempatan
                atau dalam membuat kesempatan.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/express-future/express-future-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
