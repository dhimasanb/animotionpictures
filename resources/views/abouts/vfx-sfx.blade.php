@extends('layouts.master')
@section('title', 'VFX & SFX EXPERIENCE')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/vfx-sfx/vfx-header.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>VFX & SFX EXPERIENCE</b></h2>
            <p>Perkenalkan VFX ( Vussial FX ) dan SFX ( Special FX ), adalah sebuah proses pembuatan citra sebuah
                film,
                dimana VFX dan SFX ini di buat untuk menambahkan efek-efek yang ingin di tampilkan untuk membuat
                suasana
                menjadi lebih dramatis sesuai citra sebuah film dan cerita yang telah dibuat.
            </p>
            <p>VFX dan SFX adalah elmen yang juga sangat penting, karena tanpa keduanya ini sebuah film akan terasa
                sangat
                membosankan dan tidak hidup.
            </p>
            <p>Animotion telah mengikuti perkembangan VFX dan SFX dari sebelum
                Animotion berdiri seperti sekarang ini, banyak hal yang telah di temukan dalam perjalanannya dan
                cara dalam
                meng implementasikannya.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/vfx-sfx/vfxandsfx-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
