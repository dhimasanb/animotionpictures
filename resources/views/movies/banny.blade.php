@extends('layouts.master')
@section('title', 'BANNY')
@section('section')
    <!-- Inspiro Slider -->
    <div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360">
        <!-- Slide 1 -->
        <div class="slide kenburns" data-bg-parallax="animotion/home/banny.jpg">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="slide-captions text-light">
                    <div class="row" style="padding-top:5%;">
                        <h1>BANNY</h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Slide 1 -->
    </div>
    <!--end: Inspiro Slider -->

    <!-- Content -->
    <section>
        <div class="container text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=gAetLUSYPS8"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2><b>BANNY</b></h2>
            <p>Alur cerita dalam setiap cerita pendek biasanya berpusat pada usaha-usaha mustahil yang dilakukan Banny
                untuk menangkap Para wortel, disertai dengan berbagai konflik fisik dan kerusakan materi. Mereka
                kadang-kadang terlihat dapat hidup damai berdampingan di beberapa episode (setidaknya dalam menit-menit
                pertama), jadi kadang-kadang tidak jelas mengapa Banny begitu bernafsunya mengejar Para Wortel.</p>
            <p>Banny jarang sekali sukses menangkap Para Wortel, terutama disebabkan oleh kepandaian dan kelincahan Para
                Wortel serta kecerobohan Banny sendiri. Banny biasanya mengalahkan para Wortell ketika para Wortel
                menjadi penyebab masalah atau ketika Para Wortel telah bertindak keterlaluan dan membuat Banny sangat
                marah.</p>
        </div>
    </section>
    <!-- end: Content -->

    <!-- Contact Form -->
    @include('layouts.partials.contact-form')
    <!-- end: Contact Form -->
@endsection
