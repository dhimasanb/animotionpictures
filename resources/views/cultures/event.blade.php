@extends('layouts.master')
@section('title', 'EVENT')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/event/event.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>EVENT</b></h2>
            <p>We'd love to connect with you in person at Animation Industry Events and Conferences. Please see our
                events page for details on where to find us around the world.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/event/event-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
