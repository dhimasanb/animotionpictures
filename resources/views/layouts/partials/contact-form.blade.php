<!-- Content -->
<div class="in-touch col-lg-7">
    <h2 class="text-uppercase">
        <B>STAY IN TOUCH</B>
    </h2>
    <p>Register today for the Animation Pictures newsletter to receive</p>
    <p>all the latest information about our upcoming films and projects.</p>
    <form class="widget-contact-form" novalidate action="{{ route('post.stay-in-touch')  }}" role="form" method="post">
        @csrf
        <div class="row">
            <div class="form-group col-md-12">
                <br><br>
                <h4><b>ENTER BIRTH DATE</b></h4>
                <br>
                <input type="text" aria-required="true" name="age" required
                       class="form-control required name" placeholder="UMUR">
            </div>
            <div class="col-md-12">
                <input type="email" aria-required="true" name="email" required
                       class="form-control required email" placeholder="E-MAIL">
            </div>
            <div class="box bg-4 col-md-8 p-0 my-4">
                <button
                    type="submit"
                    class="button-radius-carrer button--nanuk button--text-thick button--text-upper button--size-s">
                    <span>N</span><span>E</span><span>X</span><span>T</span><span>></span>
                </button>
            </div>
        </div>
    </form>
</div>
<!-- end: Content -->
