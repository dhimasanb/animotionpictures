@extends('layouts.master')
@section('title', 'Culture')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/culture/header-culture.jpg" alt="animotion pictures">
    </div>

    <!-- Content -->
    <section class="p-b-0">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-section text-center m-b-40" data-animate="fadeInUp">
                    <h2><b>A VIBRANT<br>COMMUNITY</b></h2>
                </div>
                <div class="row" data-animate="fadeInUp">
                    <div class="col-lg-12"><img class="img-fluid" src="animotion/culture/mid-content-page.jpg"
                                                alt="Animotion Picture"></div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row m-b-30">
                <div class="col-md-6 mb-5">
                    <h2 class="featurette-heading"><b>THE COMMUNITY</b>
                    </h2>
                    <p class="lead">More than a name, Animotion is a place. The main campus is home to an immense
                        amount of talent in a concentrated space, all thriving in a collaborative creative haven.
                    </p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('community') }}" style="color: unset;">
                            <button
                                class="button-radius-culture button--nanuk button--text-thick button--text-upper button--size-s">
                                <span>J</span><span>O</span><span>I</span><span>N</span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="" src="animotion/culture/the-community.jpg" alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6 order-md-1 m-b-5">
                    <h2 class="featurette-heading"><b>EVENT</b></h2>
                    <p class="lead">We'd love to connect with you in person at Animation Industry Events and
                        Conferences.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('event') }}" style="color: unset;">
                            <button
                                class="button-radius-culture button--nanuk button--text-thick button--text-upper button--size-s">
                                <span>M</span><span>O</span><span>R</span><span>E</span><span>&nbsp;</span><span>A</span><span>B</span><span>O</span><span>U</span><span>T</span><span>&nbsp;</span><span>E</span><span>V</span><span>E</span><span>N</span><span>T</span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-left" src="animotion/culture/event.jpg" alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6">
                    <h2 class="featurette-heading"><b>GIVING BACK</b></h2>
                    <p class="lead">We are committed to creating a diverse workforce that is dedicated to bettering
                        our industry and our surrounding communities.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('giving-back') }}" style="color: unset;">
                            <button
                                class="button-radius-culture button--nanuk button--text-thick button--text-upper button--size-s">
                                <span>M</span><span>O</span><span>R</span><span>E</span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-right" src="animotion/culture/giving-back.jpg" alt="Animotion Picture">
                </div>
            </div>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/culture/culture-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
