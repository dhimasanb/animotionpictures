@extends('layouts.master')
@section('title', 'GIVING BACK')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/vfx-sfx/vfx-header.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>GIVING BACK</b></h2>
            <p>We are committed to creating a diverse workforce that is dedicated to bettering our industry and our
                surrounding communities. We take our corporate social responsibility seriously, and in addition have
                many
                programs related to education, mentoring and
                employment for entry level and continuing
                opportunities.</p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/event/event-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
