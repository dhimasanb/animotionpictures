@extends('layouts.master')
@section('title', 'CGI : COMPUTER GENERATED IMAGERY')
@section('section')
    <!-- Inspiro Slider -->
    <div id="slider" class="inspiro-slider" data-height-xs="360">
        <!-- Slide 1 -->
        <div class="slide kenburns">
            <img src="animotion/about/cgi.jpg" alt="animotion pictures">
        </div>
        <!-- end: Slide 1 -->
    </div>
    <!--end: Inspiro Slider -->

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>CGI : COMPUTER GENERATED IMAGERY</b></h2>
            <p>Perkenalkan Teknologi yang telah berkembang hingga saat ini; CGI ( Computer Generated Imagery ). CGI
                adalah
                Teknologi yang diperkenalkan sejak 1986, yang terus berkembang hingga saat ini. Teknologi ini sering
                di gunakan
                untuk membuat sebuah karakter dalam pembuatan sebuah karya Animasi “Real”, dengan CGI sebuah
                karakter animasi
                bisa di buat semirip mungkin dengan manusia atau aktor pada dunia aslinya.
            </p>
            <p>Pembuatan sebuah karakter 3D dalam hal ini biasa di peruntukkan untuk film-film dengan gaya
                “Realistic” dimana
                sebuah aktor dan gaya “Real” di gabungkan atau di buat dengan cara yang “limitless” yang mana
                hasilnya sudah
                pasti sangat bagus dan mengagumkan, dimana segala sesuatunya bisa di lakukan tetapi tetap dengan
                nuansa seperti
                di dunia nyata.
            </p>
            <p>Animotion telah menggunakan CGI teknologi ini dengan baik dalam pembuatan sebuah karakter aktor
                terkenal dan
                legendaris, yang coba kami hidupkan kembali untuk membuat sebuah karya film yang luar biasa ini.
            </p>
            <p>Kami menerapkannya dalam beberapa Film yang sedang kami buat yang kami namakan Benyamin, Pitung dan
                Kesathria
                Nusantara, beberapa film ini
                belum secara Official Kami rilis, ada beberapa konsern yang sedang kami upayakan dalam pembuatan
                karya film ini.
            </p>
            <p>Mohon Doa dan Supportnya dari semuanya, untuk industri perfilman dan animasi indonesia.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/cgi/cgi-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
