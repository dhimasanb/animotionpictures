@extends('layouts.master')
@section('title', 'Animotion Pictures')
@section('section')
    <!-- Inspiro Slider -->
    <div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360" data-arrows="false">
        <!-- Slide 1 -->
        <div class="slide kenburns" style="background-image:url('{{ asset('animotion/home/header-home.jpg') }}');">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="slide-captions text-light">
                    <div class="row" style="padding-top:5%;">
                        <h3 style="padding-left:1%;">NOW STREAMING</h3>
                        <h1>ANIMOTION EXISTS AND WORKS</h1>
                        <div class="box bg-1">
                            <a href="{{ url('/oncomm') }}">
                                <button
                                    class="button button--aylen button--border-thick button--inverted button--text-upper button--size-s">
                                    <b>WATCH
                                        VIDEO</b></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Slide 1 -->
        <!-- Slide 2 -->
        <div class="slide kenburns"
             style="background-image:url('{{ asset('animotion/home/metaverse_readywhite-v3.jpg') }}');">
        </div>
        <!-- end: Slide 2 -->
    </div>
    <!--end: Inspiro Slider -->

    <!-- Content -->
    <section id="movie">
        <div class="container text-center">
            <h1><b>MOVIE</b></h1>
        </div>
    </section>
    <!-- end: Content -->

    <!-- movies item -->
    <div class="row-cols-12">
        <div class="portfolio-item no-overlay ct-photography ct-media ct-branding ct-Media ct-marketing ct-webdesign">
            <div class="portfolio-item-wrap">
                <div class="portfolio-slider">
                    <div class="carousel dots-inside dots-dark arrows-dark" data-items="3" data-dots="false"
                         data-loop="true" data-autoplay="true" data-animate-in="fadeIn" data-animate-out="fadeOut"
                         data-autoplay="1500">
                        <a href="{{ url('/oncomm') }}">
                            <img src="animotion/home/oncomm.jpg" alt="oncomm">
                        </a>
                        <a href="{{ url('/banny') }}">
                            <img src="animotion/home/banny.jpg" alt="banny">
                        </a>
                        <a href="{{ url('/emot-show') }}">
                            <img src="animotion/home/emot-show.jpg" alt="emot-show">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end: movies item -->

    <!-- Content -->
    <section id="works">
        <div class="container text-center">
            <h1><b>OUR WORKS</b></h1>
        </div>
    </section>
    <!-- end: Content -->

    <!-- works item -->
    <div class="row-cols-12">
        <div class="portfolio-item no-overlay ct-photography ct-media ct-branding ct-Media ct-marketing ct-webdesign">
            <div class="portfolio-item-wrap">
                <div class="portfolio-slider">
                    <div class="carousel dots-inside dots-dark arrows-dark" data-items="3" data-dots="false"
                         data-loop="true" data-autoplay="true" data-animate-in="fadeIn" data-animate-out="fadeOut"
                         data-autoplay="1500">
                        <a href="{{ url('/muspada-1') }}">
                            <img src="animotion/home/muspada-1.jpg" alt="muspada-1">
                        </a>
                        <a href="{{ url('/museum-naspro') }}">
                            <img src="animotion/home/museum-naspro.jpg" alt="museum-naspro">
                        </a>
                        <a href="{{ url('/muspada-2') }}">
                            <img src="animotion/home/muspada-2.jpg" alt="muspada-2">
                        </a>
                        <a href="{{ url('/muspada-3') }}">
                            <img src="animotion/home/muspada-3.jpg" alt="muspada-3">
                        </a>
                        <a href="{{ url('/mrt') }}">
                            <img src="animotion/home/mrt.jpg" alt="mrt">
                        </a>
                        <a href="{{ url('/kai') }}">
                            <img src="animotion/home/kai.jpg" alt="kai">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/about/about-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>

    <!-- modal movies -->
    <div id="modalOncomm" class="modal" data-delay="3000"
         style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=IgYNSPgDpt4"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">ON:COMM</h2>
            <p>ON:COMM adalah sebuah singkatan dari “On Communication” dimana dalam film serial dengan genre action
                commedy,
                ini terdiri dari 3 karakter utama, Berry, Idur, dan Verty dan karakter musuhnya seperti maskus dan
                mongkus.
            </p> <br>
            <p>ON:COMM merupakan serial animasi yang menyisipkan konten-konten edukasi di dalamnya, film animasi
                serial dengan karakter handphone dan laptop yang bisa bertransformasi menjadi robot ini
                merupakan film animasi pertama dengan style robotic dan transformasi nya.</p>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>

    <div id="modalBanny" class="modal" data-delay="3000" style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=gAetLUSYPS8"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">BANNY</h2>
            <p>Alur cerita dalam setiap cerita pendek biasanya berpusat pada usaha-usaha mustahil yang dilakukan
                Banny untuk
                menangkap Para wortel,
                disertai dengan berbagai konflik fisik dan kerusakan materi. Mereka kadang-kadang terlihat dapat
                hidup damai
                berdampingan di beberapa episode (setidaknya dalam menit-menit pertama),
                jadi kadang-kadang tidak jelas mengapa Banny begitu bernafsunya mengejar Para Wortel.</p> <br>
            <p>Banny jarang sekali sukses menangkap Para Wortel, terutama disebabkan oleh kepandaian dan kelincahan
                Para
                Wortel
                serta kecerobohan Banny sendiri.
                Banny biasanya mengalahkan para Wortell ketika para Wortel menjadi penyebab masalah atau ketika Para
                Wortel
                telah
                bertindak keterlaluan dan membuat Banny sangat marah.</p>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>

    <div id="modalEmotShow" class="modal" data-delay="3000"
         style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315"
                        src="https://www.youtube.com/channel/UCuRY2G4pKFnI87mLIg7_Lxw/channels" frameborder="0"
                        allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">EMOT</h2>
            <p>Perkenalkan EMOT SHOW, salah satu karakter di Animotion yang memiliki program “show” yang mana dalam
                Emot
                Show ini si karakter utama yaitu si Emot bisa menampilkan konten apapun di dalamnya,
                termasuk beberapa konten yang telah di perankan oleh si Emot ini yaitu Review Earphone di Channel
                Youtubenya
                “Emot Show”. Nantikan konten-konten terbaru yang akan dibuat si Emot ya.</p>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>
    <!-- end: modal movies item -->

    <!-- Modal works item -->
    <div id="modalMuspada1" class="modal" data-delay="3000"
         style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=2S6kMdJDoiU"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">PUTRA & PUTRI 1 </h2>
            <p>film animasi putra dan putri ini adalah sebuah film pendek yang dibuat untuk meng-edukasi masyarakat
                dan
                juga melestarikan sejarah negeri dengan sajian yang berbeda sehingga diharapkan
                dapat menarik perhatian masyarakat atau khususnya para pelajar dalam mempelajari sejarah bangsa
                dan tertarik mengunjungi museum sumpah pemuda.
            </p> <br>
            <p>Dan terbukti efektif Ketika peluncurannya (animasi Putra putri) ini menarik banyak pengunjung
                anak-anak
                sekolah ke museum sumpah pemuda tersebut.
            </p>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>
    <div id="modalMuseumnaspro" class="modal" data-delay="3000"
         style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=2S6kMdJDoiU"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">MUSEUM NASPRO</h2>
            <p>film animasi Museum Naspro ini adalah sebuah film pendek yang dibuat untuk meng-edukasi masyarakat
                dan
                juga melestarikan sejarah negeri dengan sajian yang berbeda sehingga diharapkan
                dapat menarik perhatian masyarakat atau khususnya para pelajar dalam mempelajari sejarah bangsa
                dan tertarik mengunjungi museum naskah proklamasi ini.
            </p> <br>
            <p>Dan terbukti efektif Ketika peluncurannya (animasi Putra putri) ini menarik banyak pengunjung
                anak-anak
                sekolah ke museum sumpah pemuda tersebut.
            </p>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>
    <div id="modalMuspada2" class="modal" data-delay="3000"
         style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=RNGM1PkVG9Q"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">PUTRA & PUTRI 2 </h2>
            <p>film animasi putra dan putri ini adalah sebuah film pendek yang dibuat untuk meng-edukasi masyarakat
                dan
                juga melestarikan sejarah negeri dengan sajian yang berbeda sehingga diharapkan
                dapat menarik perhatian masyarakat atau khususnya para pelajar dalam mempelajari sejarah bangsa
                dan tertarik mengunjungi museum sumpah pemuda.
            </p> <br>
            <p>Dan terbukti efektif Ketika peluncurannya (animasi Putra putri) ini menarik banyak pengunjung
                anak-anak
                sekolah ke museum sumpah pemuda tersebut.
            </p>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>
    <div id="modalMuspada3" class="modal" data-delay="3000"
         style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=jasm4fhItK0"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">PUTRA & PUTRI 3 </h2>
            <p>film animasi putra dan putri ini adalah sebuah film pendek yang dibuat untuk meng-edukasi masyarakat
                dan
                juga melestarikan sejarah negeri dengan sajian yang berbeda sehingga diharapkan
                dapat menarik perhatian masyarakat atau khususnya para pelajar dalam mempelajari sejarah bangsa
                dan tertarik mengunjungi museum sumpah pemuda.
            </p> <br>
            <p>Dan terbukti efektif Ketika peluncurannya (animasi Putra putri) ini menarik banyak pengunjung
                anak-anak
                sekolah ke museum sumpah pemuda tersebut.
            </p>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>
    <div id="modalMrt" class="modal" data-delay="3000" style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=xvMOn9rMcmo"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">MRT</h2>
            <p>Projek sosialisasi proses pembangunan MRT</p> <br>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>
    <div id="modalKai" class="modal" data-delay="3000" style="max-width: 700px; min-height:380px">
        <div class="p-t-30 text-center">
            <div class="iframe-wrap m-b-20">
                <iframe id="youtube" width="560" height="315" src="https://www.youtube.com/watch?v=LKzUM1_ZCQw"
                        frameborder="0" allowfullscreen></iframe>
            </div>
            <h2 class="m-b-0">KAI</h2>
            <p>Projek sosialisasi stasiun KAI</p> <br>
            <a class="btn btn-light modal-close" href="#">Dismiss</a>
        </div>
    </div>
    <!-- end: Modal works item -->
@endsection
