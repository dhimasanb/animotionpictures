@extends('layouts.master')
@section('title', 'THE COMMUNITY')
@section('section')
    <div class="container">
        <img class="img-fluid" src="animotion/the-community/the-community.jpg" alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>THE COMMUNITY</b></h2>
            <p>More than a name, Animotion is a place. The main campus is home to an immense amount of talent in a
                concentrated
                space, all thriving in a collaborative creative haven.
            </p>
            <p>We are a diverse community of filmmakers, animators, designers, artists, innovators, technologists
                and executives
                who stay at the top of their field by taking risks and pushing beyond the limits in every aspect of
                their work.
            </p>
            <p>Our leadership is committed to providing ongoing education and training programs in addition to
                encouraging
                employees to engage and unwind with a range of social offerings from artist workshops, screenings,
                and speaker
                series, to gallery exhibits, clubs, yoga and ping pong.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/the-community/the-community-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
