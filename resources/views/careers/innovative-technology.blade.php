@extends('layouts.master')
@section('title', 'INNOVATIVE TECHNOLOGY')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/innovative-tech/innovative-tech-header.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>INNOVATIVE TECHNOLOGY</b></h2>
            <p>Animotion adalah perusahaan Animasi yang tidak hanya fokus berinovasi dalam sebuah karya animasi,
                tapi juga berinovasi dalam membangun sebuah engine atau teknologi dalam membuat sebuah karya film
                dan animasi.
            </p>
            <p>Dari tahun ke tahun Animotion selalu merancang dan membuat sebuah inovasi teknologi yang akan
                menunjang dalam sebuah pembuatan karya - karya film dan animasinya.
            </p>
            <p>dan dengan ini kami sangat berharap bisa membantu mensupport para pelaku industri film dan animasi
                ini.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/innovative-tech/innovative-tech-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
