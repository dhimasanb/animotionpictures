<footer id="footer" class="inverted">
    <div class="footer-sosmed background-black text-center">
        <div class="row mx-lg-n5">
            <div class="col-lg-12 text-center">
                <img src="animotion/Logo_animotion_incolor.png" class="client-logo-grayscale" width="300px"
                     height="140px" class="logo-default">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <nav class="footer-link text-center">
                    <ul>
                        <li><a href="{{ route('home') }}">HOME</a></li>
                        <li><a href="{{ route('home') }}#movie">MOVIE</a></li>
                        <li><a href="{{ route('home') }}#works">OUR WORKS</a></li>
                        <li><a href="{{ route('career') }}">CAREERS</a></li>
                        <li><a href="{{ route('culture') }}">CULTURE</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        {{--        <nav class="client-logo">--}}
        {{--            <div class="logo-grayscale">--}}
        {{--                <div class="col-lg-12 text-center">--}}
        {{--                    <nav class="footer-link text-white">--}}
        {{--                        <br>--}}
        {{--                        <ul>--}}
        {{--                            <li>FOLLOW ANIMOTION</li>--}}
        {{--                        </ul>--}}
        {{--                    </nav>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <ul>--}}
        {{--                <li><img class="logo-grayscale" src="animotion/Button_color-11.png" width="30px" height="30px"></li>--}}
        {{--                <li><img class="logo-grayscale" src="animotion/Button_color-12.png" width="30px" height="30px"></li>--}}
        {{--                <li><img class="logo-grayscale" src="animotion/Button_color-13.png" width="35px" height="30px"></li>--}}
        {{--                <li><img class="logo-grayscale" src="animotion/Button_color-14.png" width="35px" height="30px"></li>--}}
        {{--            </ul>--}}
        {{--        </nav>--}}
        <div class="copyright-footer background-black">
            <div class="container">
                <div class="copyright-text text-center text-white">&copy;
                    <script>document.write(new Date().getFullYear())</script>
                    ANIMOTION ALL RIGHTS RESERVED. TERMS AND
                    CONDITIONS | PRIVACY POLICY - NEW | CA NOTICE | YOUTUBE TERMS OF SERVICE | GOOGLE PRIVACY POLICY
                </div>
            </div>
        </div>
    </div>
</footer>
