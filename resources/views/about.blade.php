@extends('layouts.master')
@section('title', 'About')
@section('section')
    <!-- Inspiro Slider -->
    <div id="slider" class="inspiro-slider slider-fullscreen dots-creative" data-height-xs="360" style="margin-bottom: 1rem">
        <!-- Slide 1 -->
        <div class="slide kenburns" style="background: url('{{ asset('animotion/about/header-about.jpg') }}')">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="slide-captions text-light">
                    <div class="row" style="padding-top:5%;">
                        <div class="page-title">
                            <h4><b>ABOUT ANIMOTION</b></h4>
                            <h1><b>WHO WE ARE</b></h1>
                            <span><b>we tell the stories with thousand imagination make it real and live to
                                            tell the stories to you.</b></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Slide 1 -->
    </div>
    <!--end: Inspiro Slider -->

    <!-- Content -->
    <section class="p-b-0" style="padding-top: 0;">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-section text-center m-b-40" data-animate="fadeInUp">
                    <h2><b>IT HAS BEEN 14 YEARS, <br/>
                            ANIMOTION EXISTS AND
                            WORKS.</b></h2>
                </div>
                <div class="row" data-animate="fadeInUp">
                    <div class="col-lg-12"><img class="img-fluid" src="animotion/about/mid-content-page.jpg"
                                                alt="Animotion Picture"></div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row m-b-30">
                <div class="col-md-6">
                    <h2 class="featurette-heading"><b>INOVATION <br/>
                            ANIMATION.</b>
                    </h2>
                    <p class="lead">Bertahun-tahun animasi telah bertransformasi menjadi banyak bentuk dan dimensi,
                        namun tidak hanya
                        itu. Sebuah karya Animasi juga disajikan dengan gaya yang berbeda-beda, dan kami memiliki
                        itu.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('innovation') }}" style="color: unset;">
                            <button
                                class="button-radius button--nanuk button--text-thick button--text-upper button--size-s button--border-thick">
                                <span>M</span><span>O</span><span>R</span><span>E</span><span>></span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-right" src="animotion/about/innovation-animation.jpg" alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6 order-md-1 m-b-5">
                    <h2 class="featurette-heading"><b>CGI : COMPUTER
                            GENERATED IMAGERY</b></h2>
                    <p class="lead">Perkenalkan Teknologi yang telah berkembang hingga saat ini; CGI ( Computer
                        Generated Imagery ).
                        CGI adalah Teknologi yang diperkenalkan sejak 1986, yang terus berkembang hingga saat ini.
                        Teknologi ini
                        sering di gunakan untuk membuat sebuah karakter dalam pembuatan sebuah karya Animasi “Real”,
                        dengan CGI
                        sebuah karakter animasi bisa di buat semirip mungkin dengan manusia atau aktor pada dunia
                        aslinya.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('cgi') }}" style="color: unset;">
                            <button
                                class="button-radius button--nanuk button--text-thick button--text-upper button--size-s button--border-thick">
                                <span>M</span><span>O</span><span>R</span><span>E</span><span>></span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-left" src="animotion/about/cgi.jpg" alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6">
                    <h2 class="featurette-heading"><b>NEW TECHNIQUE
                            ILLUSTRATOR &
                            ENVIRONMENT</b></h2>
                    <p class="lead">Dalam sebuah karya Film “Environment Set” adalah menjadi hal yang juga penting
                        untuk di rancang, guna
                        mendukung maksud dari cerita yang akan di sajikan dalam bentuk sebuah karya Film atau
                        animasi, tanpa sebuah
                        “Environment” sebuah film akan sangat terasa tidak lengkap dan rasa yang ingin di sampaikan
                        menjadi aneh.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('new-technique') }}" style="color: unset;">
                            <button
                                class="button-radius button--nanuk button--text-thick button--text-upper button--size-s button--border-thick">
                                <span>M</span><span>O</span><span>R</span><span>E</span><span>></span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-right" src="animotion/about/newIllustrator-technique.jpg"
                         alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6 order-md-1 m-b-5">
                    <h2 class="featurette-heading"><b>VFX & SFX EXPERIENCE</b></h2>
                    <p class="lead">Perkenalkan VFX ( Vussial FX ) dan SFX ( Special FX ), adalah sebuah proses
                        pembuatan citra sebuah
                        film, dimana VFX dan SFX ini di buat untuk menambahkan efek-efek yang ingin di tampilkan
                        untuk membuat suasana
                        menjadi lebih dramatis sesuai citra sebuah film dan cerita yang telah dibuat.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('vfx-sfx') }}" style="color: unset;">
                            <button
                                class="button-radius button--nanuk button--text-thick button--text-upper button--size-s button--border-thick">
                                <span>M</span><span>O</span><span>R</span><span>E</span><span>></span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-left" src="animotion/about/vfx-sfx.jpg" alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6">
                    <h2 class="featurette-heading"><b>BEAM RENDERING
                            ENGINE & TECHNIQUE</b></h2>
                    <p class="lead">Perkenalkan BEAM Rendering Engine, adalah sebuah Render Engine yang juga di
                        dukung dengan teknik sistem
                        untuk mengoptimalkan sebuah hasil render dengan waktu yang relatif cepat.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('beam-rendering') }}" style="color: unset;">
                            <button
                                class="button-radius button--nanuk button--text-thick button--text-upper button--size-s button--border-thick">
                                <span>M</span><span>O</span><span>R</span><span>E</span><span>></span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-right" src="animotion/about/beam-render.jpg" alt="Animotion Picture">
                </div>
            </div>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/about/about-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
@push('styles')
    <style>
        .flickity-slider {
            height: 90%;
        }

        .kenburns-bg.kenburns-bg-animate {
            transform: none !important;
        }
    </style>
@endpush
