<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="identifier-url" content="https://animotionpictures.id" />
    <meta name="title" content="Animation Pictures" />
    <meta name="description" content="studio animasi anak bangsa, telah berdiri sejak 2006, berkarya dalam banyak hal, film animasi sampai iklan. Animotionpictures telah banyak di percaya dalam pengerjaan iklan di indonesia sejak berdirinya pada tahun 2006." />
    <meta name="abstract" content="studio animasi anak bangsa, telah berdiri sejak 2006, berkarya dalam banyak hal, film animasi sampai iklan. Animotionpictures telah banyak di percaya dalam pengerjaan iklan di indonesia sejak berdirinya pada tahun 2006." />
    <meta name="keywords" content="studio animasi, film animasi, animasi indonesia" />
    <meta name="author" content="Animation Pictures" />
    <meta name="revisit-after" content="15" />
    <meta name="language" content="EN" />
    <meta name="copyright" content="© 2021 Animation Pictures" />
    <meta name="robots" content="All" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <!-- Document title -->
    <title>@yield('title')</title>
    <!-- Stlesheets & Fonts -->
    @include('layouts.partials.css')
</head>

<body>
<!-- Body Inner -->
<div class="body-inner">

    <!-- New Header -->
    <header id="header" data-fullwidth="true" class="">
        <div class="header-inner">
            <div class="container">
                <!--Logo-->
                <div id="logo">
                    <a href="{{ route('home') }}">
                        <img src="animotion/Logo_animotion_incolor.png" class="logo-default">
                        <img src="animotion/Logo_animotion_incolor.png" class="logo-sticky">
                    </a>
                </div>
                <!--End: Logo-->
                <!--Navigation Resposnive Trigger-->
                <div id="iconMenu-trigger">
                    <img class="iconMenu" src="animotion/Button_menu-16.png" width="20px" height="20px">
                </div>
                <!--end: Navigation Resposnive Trigger-->
                <!--Navigation-->
                <div id="mainMenu" class="" style="min-height: 0px;">
                    <div class="container">
                        <nav>
                            <ul>
                                {{--                                <li class="social_menu">--}}
                                {{--                                    <img class="logo-grayscale" src="animotion/Button_color-11.png" width="20px"--}}
                                {{--                                         height="20px">--}}
                                {{--                                </li>--}}
                                {{--                                <li class="social_menu">--}}
                                {{--                                    <img class="logo-grayscale" src="animotion/Button_color-12.png" width="20px"--}}
                                {{--                                         height="20px">--}}
                                {{--                                </li>--}}
                                {{--                                <li class="social_menu">--}}
                                {{--                                    <img class="logo-grayscale" src="animotion/Button_color-13.png" width="25px"--}}
                                {{--                                         height="20px">--}}
                                {{--                                </li>--}}
                                {{--                                <li class="social_menu">--}}
                                {{--                                    <img class="logo-grayscale" src="animotion/Button_color-14.png" width="25px"--}}
                                {{--                                         height="20px">--}}
                                {{--                                </li>--}}
                                <li>
                                    <img class="iconMenu" src="animotion/Button_menu-16.png" width="20px" height="20px">
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--end: Navigation-->
            </div>
        </div>
    </header>
    <!-- end: New Header -->

    <!-- Header -->
    <header id="header" class="dark header-menu" data-fullwidth="true">
        <div class="header-inner background-black">
            <div class="container">
                <!--Navigation-->
                <div id="mainMenu background-black" class="menu-center">
                    <div class="container-color">
                        <nav>
                            <a href="{{ route('about') }}" class="btn-menu"><b>ABOUT</b></a>
                            <a href="{{ route('career') }}" class="btn-menu"><b>CAREER</b></a>
                            <a href="{{ route('culture') }}" class="btn-menu"><b>CULTURE</b></a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--end: Navigation-->
    </header>
    <!-- end: Header -->

    @yield('section')

</div>
<!-- end: portfolio item -->

<!-- Client Footer -->
<div class="footer-content background-black">
    <div class="container-client text-center text-white">
        <h2><b>CLIENT & PARTNERS</b></h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="carousel client-logos" data-items="6" data-dots="false">
                <a href="#"><img alt="" class="client-logo-grayscale client-logo-size"
                                 src="animotion/logo-museum-sumpah-pemuda.png">
                </a>
                <a href="#"><img alt="" class="client-logo-grayscale client-logo-size"
                                 src="animotion/logo-museum-kebangkitan-nasional.png">
                </a>
                <a href="#"><img alt="" class="client-logo-grayscale client-logo-size" src="animotion/logo-mandiri.png">
                </a>
                <a href="#"><img alt="" class="client-logo-grayscale client-logo-size"
                                 src="animotion/logo-bca-finance.png">
                </a>
                <a href="#"><img alt="" class="client-logo-grayscale client-logo-size"
                                 src="animotion/logo-krakatau-steel.png">
                </a>
                <a href="#"><img alt="" class="client-logo-grayscale client-logo-size"
                                 src="animotion/logo-mrt-jakarta.png">
                </a>
            </div>
        </div>
    </div>
</div>
</div>
<!-- end: Client Footer -->

<!-- Footer -->
@include('layouts.partials.footer')
<!-- end: Footer -->
<!-- Scroll top -->
<a id="scrollTop"><i class="icon-chevron-up"></i><i class="icon-chevron-up"></i></a>

<!-- Javascript -->
@include('layouts.partials.js')

</body>

</html>
