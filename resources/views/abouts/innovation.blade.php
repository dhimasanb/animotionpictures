@extends('layouts.master')
@section('title', 'INOVATION ANIMATION.')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/innovation-animation/innovation-animation-about.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>INNOVATION ANIMATION</b></h2>
            <p>Bertahun-tahun animasi telah bertransformasi menjadi banyak bentuk dan dimensi, namun tidak hanya
                itu. Sebuah karya Animasi juga disajikan dengan gaya yang berbeda-beda, dan kami memiliki itu.
            </p>
            <p>Animotion Telah membuat banyak karya, film,iklan dan beberapa karya campaign yang di buat dalam
                bentuk Animasi.
            </p>
            <p>Berinovasi dan berkarya dalam sebuah Animasi adalah sebuah karya yang tidak memiliki batas.
            </p>
            <p>Animotion menjadikan “Limitless” dalam animasi ini sebagai tempat dimana kami membangun dan membuat
                sebuah cerita untuk dinikmati banyak mata dan di dengar banyak telinga, sehingga bisa membentuk
                sebuah rasa dalam sebuah karya Entertainment.
            </p>
            <p>Bahkan tidak hanya untuk hiburan, animasi bisa di gunakan untuk berbagai kebutuhan dalam dunia
                pendidikan, sosialisasi kebudayaan dan lain sebagainya, sebuah karya yang hidup tanpa batasan.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/innovation-animation/innovation-animation-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
