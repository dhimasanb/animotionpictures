@extends('layouts.master')
@section('title')
    {{ $name }}
@endsection
@section('section')
    <div class="iframe-wrap m-b-20" style="padding-top: {{ $paddingTop ?? '56.25%' }}; position: relative; overflow: hidden;">
        <iframe frameborder="0" allowfullscreen="" allow="autoplay;fullscreen" scrolling="no"
                src="{{ $video }}"
                style="position: absolute; height: 100%; width: 100%; left: 0px; top: 0px;">
        </iframe>
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>{{ $name }}</b></h2>
            {!! $description !!}
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/about/about-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>
@endsection
