@extends('layouts.master')
@section('title', 'EARLY CAREERS PROGRAM')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/early-program/early-program.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>EARLY CAREERS PROGRAM</b></h2>
            <p>We are always looking for up and coming animation talent, so we created several programs to immerse,
                mentor and train that talent for the next wave of storytellers, artists and producers to bring our
                characters and stories to life. Our 3 programs include DreamWorks Animation Internship Program, TV
                Story & Design Trainee Program, and Feature Animation Artistic Development Program. Please check the
                site for application details and program guidelines.
            </p>
            <p>Recent graduate, current student, artist pivoting career genres, self taught? Please explore our
                early career positions and programs.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/early-program/early-program-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
