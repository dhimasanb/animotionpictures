@extends('layouts.master')
@section('title', 'BEAM RENDERING ENGINE & TECHNIQUE')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/beam-rendering-engine/beam.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>BEAM RENDERING ENGINE AND TECHNIQUE</b></h2>
            <p>Perkenalkan BEAM Rendering Engine, adalah sebuah Render Engine yang juga di dukung dengan teknik
                sistem untuk mengoptimalkan sebuah hasil render dengan waktu yang relatif cepat.
            </p>
            <p>BEAM Rendering Engine adalah sebuah produk render dari Animotion, yang sering di pergunakan dalam
                tahap Rendering, yang mana proses Rendering ini adalah final proses dalam pembuatan sebuah karya
                animasi 3 Dimensi, Rendering adalah proses finalisasi sebuah gambar, yang menentukan hasil akhirnya.
            </p>
            <p>Maka dari itu Animotion sangat menganggap penting proses rendering ini.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/beam-rendering-engine/beam-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
