@extends('layouts.master')
@section('title', 'Career')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/career/header-career.jpg" alt="animotion pictures">
    </div>

    <!-- Content -->
    <section class="p-b-0">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-section text-center m-b-40" data-animate="fadeInUp">
                    <h2><b>CAREERS AT<br/>
                            ANIMOTION</b></h2>
                </div>
                <div class="row" data-animate="fadeInUp">
                    <div class="col-lg-12"><img class="img-fluid" src="animotion/career/mid-content-page.jpg"
                                                alt="Animotion Picture"></div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row m-b-30">
                <div class="col-md-6 mb-4">
                    <h2 class="featurette-heading"><b>CURRENT OPENING</b>
                    </h2>
                    <p class="lead">We are looking for passionate, driven people with a love for animated
                        storytelling to
                        become DreamWorkers!</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('current-opening') }}" style="color: unset;">
                            <button
                                class="button-radius-carrer button--nanuk button--text-thick button--text-upper button--size-s button--border-thick">
                                <span>B</span><span>R</span><span>O</span><span>W</span><span>S</span><span>E</span><span>&nbsp;</span><span>O</span><span>P</span><span>E</span><span>N</span><span>&nbsp;</span><span>P</span><span>O</span><span>S</span><span>I</span><span>T</span><span>I</span><span>O</span><span>N</span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="" src="animotion/career/content-current-opening.jpg"
                         alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6 order-md-1 m-b-5">
                    <h2 class="featurette-heading"><b>EARLY CAREERS<br>PROGRAM</b></h2>
                    <p class="lead">We are always looking for up and coming animation talent, so we created several
                        programs
                        to immerse, mentor and train that talent for the next wave of storytellers, artists and
                        producers to
                        bring our characters and stories to life.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('early-career') }}" style="color: unset;">
                            <button
                                class="button-radius-carrer button--nanuk button--text-thick button--text-upper button--size-s button--border-thick">
                                <span>V</span><span>I</span><span>E</span><span>W</span><span>&nbsp;</span><span>C</span><span>A</span><span>R</span><span>E</span><span>E</span><span>R</span><span>&nbsp;</span><span>O</span><span>P</span><span>P</span><span>O</span><span>R</span><span>T</span><span>U</span><span>N</span><span>I</span><span>T</span><span>I</span><span>E</span><span>S</span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-left" src="animotion/career/content-early-career-program.jpg"
                         alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6">
                    <h2 class="featurette-heading"><b>EXPRESS FUTURE<br>INTEREST</b></h2>
                    <p class="lead">Sebagai perusahaan yang bergerak dalam industri kreatif Animotion juga bergerak
                        dan
                        aktif membangun
                        serta menghidupkan organisasi kreatif dan membangun generasi kreatif untuk lebih maju
                        kedepannya.
                    </p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('express-future') }}" style="color: unset;">
                            <button
                                class="button-radius-carrer button--nanuk button--text-thick button--text-upper button--size-s button--border-thick">
                                <span>E</span><span>X</span><span>P</span><span>R</span><span>E</span><span>S</span><span>S</span><span>&nbsp;</span><span>I</span><span>N</span><span>T</span><span>E</span><span>R</span><span>E</span><span>S</span><span>T</span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-right" src="animotion/career/content-express-future-interest.jpg"
                         alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30 ">
                <div class="col-md-6 order-md-1 m-b-5">
                    <h2 class="featurette-heading"><b>FREQUENTLY ASKED<br>QUESTIONS</b></h2>
                    <p class="lead">Daftar pertanyaan yang sering diajukan dan juga informasi yang dibutuhkan terkait
                        Animotion Pictures.</p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('faq') }}" style="color: unset;">
                            <button
                                class="button-radius-carrer button--nanuk button--text-thick button--text-upper button--size-s">
                                <span>V</span><span>I</span><span>E</span><span>W</span><span>&nbsp;</span><span>F</span><span>A</span><span>Q</span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="img-fluid-left" src="animotion/career/content-frequently-asked.jpg"
                         alt="Animotion Picture">
                </div>
            </div>

            <div class="row m-b-30">
                <div class="col-md-6 mb-4">
                    <h2 class="featurette-heading"><b>INNOVATIVE<br>TECHNOLOGY</b></h2>
                    <p class="lead">Animotion adalah perusahaan Animasi yang tidak hanya fokus berinovasi dalam
                        sebuah karya
                        animasi, tapi juga berinovasi dalam membangun sebuah engine atau teknologi dalam membuat
                        sebuah
                        karya film dan animasi.
                    </p>
                    <div class="box bg-2 col-md-8 p-0">
                        <a href="{{ route('innovative-technology') }}" style="color: unset;">
                            <button
                                class="button-radius-carrer button--nanuk button--text-thick button--text-upper button--size-s">
                                <span>M</span><span>O</span><span>R</span><span>E</span><span>&nbsp;</span><span>O</span><span>U</span><span>R</span><span>&nbsp;</span><span>T</span><span>E</span><span>C</span><span>H</span><span>N</span><span>O</span><span>L</span><span>O</span><span>G</span><span>Y</span>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="col-md-15">
                    <img class="" src="animotion/career/content-innovative-technology.jpg"
                         alt="Animotion Picture">
                </div>
            </div>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/career/career-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
