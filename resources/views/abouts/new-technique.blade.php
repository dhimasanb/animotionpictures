@extends('layouts.master')
@section('title', 'NEW TECHNIQUE ILLUSTRATOR & ENVIRONMENT')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/new-technique-illustrator/new-tech-header.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>NEW TECHNIQUE ILLUSTRATOR & ENVIRONMENT</b></h2>
            <p>Dalam sebuah karya Film “Environment Set” adalah menjadi hal yang juga penting untuk di rancang, guna
                mendukung maksud dari cerita yang akan di sajikan dalam bentuk sebuah karya Film atau animasi, tanpa
                sebuah “Environment” sebuah film akan sangat terasa tidak lengkap dan rasa yang ingin di sampaikan
                menjadi aneh.
            </p>
            <p>Dalam pembuatan Environment Sebuah ketepatan dan keserasian dengan cerita atau rasa yang akan di
                sampaikan adalah sebuah point, maka dibutuhkan sebuah teknik yang benar-benar telah terancang dan
                terbukti dalam pengalaman berkarya.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/new-technique-illustrator/new-tech-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
