@extends('layouts.master')
@section('title', 'CURRENT OPENING.')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/current-opening/current-opening.jpg"
             alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>CURRENT OPENING</b></h2>
            <p>We are looking for passionate, driven people with a love for animated storytelling to become
                DreamWorkers! We typically hire in the areas of Art, Visual Development, Character Design, Story,
                Editorial, Modeling, Character Rigging, Surfacing, Layout / Previsualization, Final Layout,
                Animation, Crowds, Character Effects, Effects, Lighting, Matte Painting, Image Finaling, Technical
                Directors, and Production Management. Please check out our current open positions.
            </p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/current-opening/current-opening-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
