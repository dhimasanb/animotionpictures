@extends('layouts.master')
@section('title')
    Emot
@endsection
@section('section')
    <div class="container">
        <a href="https://www.youtube.com/channel/UCuRY2G4pKFnI87mLIg7_Lxw" target="_blank">
            <img class="img-fluid" src="animotion/emot/header-emot.jpg" alt="animotion pictures">
        </a>
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>Emot</b></h2>
            <p>Perkenalkan EMOT SHOW, salah satu karakter di Animotion yang memiliki program “show” yang mana dalam
                Emot
                Show ini si karakter utama yaitu si Emot bisa menampilkan konten apapun di dalamnya,
                termasuk beberapa konten yang telah di perankan oleh si Emot ini yaitu Review Earphone di Channel
                Youtubenya
                “Emot Show”. Nantikan konten-konten terbaru yang akan dibuat si Emot ya.</p>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/about/about-bawah.jpg" alt="Animotion Picture">
            </div>
        </div>
    </div>
@endsection
