@extends('layouts.master')
@section('title', 'FREQUENTLY ASKED QUESTIONS')
@section('section')
    <div class="flex">
        <img class="img-fluid" src="animotion/frequently-asked/frequently-asked.jpg" alt="animotion pictures">
    </div>

    <!-- Content -->
    <section>
        <div class="container text-center">
            <h2><b>FREQUENTLY ASKED QUESTIONS</b></h2>
            <div class="col-lg">
                <div class="accordion toggle fancy radius clean">
                    @foreach($faqs as $faq)
                        <div class="ac-item" style="background-color: #f0f0f0;">
                            <h5 class="ac-title" style="color: #26b9ff;">{{ $faq['question'] }}</h5>
                            <div class="ac-content" style="letter-spacing: 0;">{!! $faq['answer'] !!}</div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- end: Content -->

    <div class="container">
        <div class="row m-b-30">
            <div class="col-md-6">
            @include('layouts.partials.contact-form')
            <!-- end: Contact Form -->
            </div>
            <div class="col-md-6">
                <img class="img-fluid-right" src="animotion/frequently-asked/frequently-bawah.jpg"
                     alt="Animotion Picture">
            </div>
        </div>
    </div>

@endsection
