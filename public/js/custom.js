/*

1. Add your custom JavaScript code below
2. Place the this code in your template:



*/
$(function () {
    var $header = $('#header')
    var $menu = $('.header-menu')
    var $slider = $('#slider, .img-fluid, .iframe-wrap')
    var $btnMenu = $('.iconMenu')
    var $menuTrigger = $('#iconMenu-trigger')

    $menu.hide();
    $header.mouseenter(function () {
        $menu.slideDown('slow');
        $btnMenu.attr('src', 'animotion/Button_menu-15.png');
    })

    $slider.mouseenter(function () {
        $menu.slideUp('slow');
        $btnMenu.attr('src', 'animotion/Button_menu-16.png');
    })

});
